package herxion.assignmnet1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import org.json.JSONException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity {

    LocationManager locationmanager;
    Location location;
    SharedPreferences saved_values;
    SharedPreferences.Editor editor;
    double latitude;
    double longitude;
    double altitude;
    List<Address> addresses = null;
    String locName;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
    }

    /**
     * http://developer.android.com/guide/topics/data/data-storage.html
     * Used to get sharedPreferences to work
     */
    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences saved_values = getSharedPreferences("name", 0);
        if(saved_values != null) {
            latitude = Double.longBitsToDouble(saved_values.getLong("lat", 0));
            longitude = Double.longBitsToDouble(saved_values.getLong("lon", 0));
            putMarker(latitude, longitude);
        }
    }

    protected void onStop(){
        super.onStop();
        saved_values = getSharedPreferences("name", 0);
        editor = saved_values.edit();
        editor.putLong("lon", Double.doubleToRawLongBits(longitude));
        editor.putLong("lat", Double.doubleToRawLongBits(latitude));
        editor.apply();
    }

    public void secondActivity(final View v){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("Location", locName);
        intent.putExtra("Altitude", altitude);
        startActivity(intent);
    }

    public void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        }
    }

    public void putMarker(double lat, double lon) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("Marker").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
    }

    /**
     * http://stackoverflow.com/questions/2227292/how-to-get-latitude-and-longitude-of-the-mobile-device-in-android
     * Used to learn about the location manager
     * https://developer.android.com/training/location/display-address.html#fetch-address
     * Used to fetch address name from lat and long
     */
    public void setUpMap(final View v) throws IOException, JSONException {
        locationmanager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = locationmanager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if(location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            altitude = location.getAltitude();
            putMarker(latitude, longitude);
        }
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        locName = addresses.get(0).getLocality();
    }
}
