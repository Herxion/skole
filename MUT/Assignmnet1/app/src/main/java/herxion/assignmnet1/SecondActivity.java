package herxion.assignmnet1;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class SecondActivity extends Activity {

    public static String URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    public String name;
    public String metric = "&units=metric";
    public double altitude;
    public String temperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        LongOperation thing = new LongOperation();
        thing.execute("");
        try {
            temperature = thing.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        setup();
    }

    public void setup(){
        Intent intent = getIntent();
        name = intent.getStringExtra("Location");
        altitude = intent.getDoubleExtra("Altitude", altitude);
        TextView txt = (TextView) findViewById(R.id.coordinates);
        txt.setText("You are in " + name + ", currently " + altitude + " meters above the sea level, and the local temperature is " + temperature + " degrees Celcius");
    }

    /**
     * http://www.androidhive.info/2012/01/android-json-parsing-tutorial/
     * http://stackoverflow.com/questions/9671546/asynctask-android-example
     * http://developer.android.com/reference/java/net/URLConnection.html
     * Used to get UrlConnection, parsing JSON and AsyncTask to work
     */
    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String result;
            StringBuilder sb = new StringBuilder();
            //URL url = null;
            try {
                URL url = new URL(URL + name + metric);
                //HttpURLConnection con = null;
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                //InputStream in = null;
                InputStream in = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                result = sb.toString();
                JSONObject jObj = new JSONObject(result);
                JSONObject mainObj = jObj.getJSONObject("main");
                Double temp = mainObj.getDouble("temp");
                return ""+temp;
            }catch(Exception e){
                e.printStackTrace();
            }
            return "";
        }
    }
}
